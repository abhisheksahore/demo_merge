/* 
get all player name of all ipl season */
const fs = require('fs')

function getAllPlayer(deliveries){
    return deliveries.reduce((players, data) => {
        if (!(data.batsman in players)) players[data.batsman] = Object.keys(players).length + 1
        if (!(data.non_striker in players)) players[data.non_striker] = Object.keys(players).length + 1
        if (!(data.bowler in players)) players[data.bowler] = Object.keys(players).length + 1
        if (!(data.fielder in players)) players[data.fielder] = Object.keys(players).length + 1
        
        return players
    },{})
}

/* 
get all umpires in ipl all seasons */
function getAllUmpires(matches){
    return matches.reduce((umpires, data) => {
        if (!(data.umpire1 in umpires)) umpires[data.umpire1] = Object.keys(umpires).length + 1
        if (!(data.umpire2 in umpires)) umpires[data.umpire2] = Object.keys(umpires).length + 1
        if (!(data.umpire3 in umpires)) umpires[data.umpire3] = Object.keys(umpires).length + 1
        return umpires
    },{})
}

/* 
get all season satadium */
function getAllStadium(matches){
    return matches.reduce((stadiums, data) => {
        if (!(data.venue in stadiums)) stadiums[data.venue] = data.city
        return stadiums
    }, {})
}

/* 
get all matches info */
function getAllMatches(matchesData, players, stadiums, umpires, team){
    return matchesData.reduce((matches, data) =>{
        const temp = [data.id, data.season, data.date, team[data.team1], team[data.team1]]
    },[])
}

async function main(){
    const data = await fs.promises.readFile('./data/raw/deliveries.json','utf-8');
    const matchesData = await fs.promises.readFile('./data/raw/matches.json','utf-8');
    // console.log(getAllPlayer(JSON.parse(data)))
    // console.log(getAllUmpires(JSON.parse(matchesData)))
    console.log(getAllStadium(JSON.parse( matchesData)));
    // console.log(JSON.parse(data)[0]);
}

main();